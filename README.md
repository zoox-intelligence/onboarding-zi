Roteiro de Ambientação para Desenvolvimento
===========================================

Este roteiro traz informações essenciais para que um membro novo da equipe
do Zoox Intelligence consiga subir uma área de trabalho no menor tempo
possível.

## Nossa Pilha (*Stack*)

Segue uma lista de *softwares* que usamos:

* O sistema operacional usado é o **Linux** e usamos as variações **Linux Mint**
  e **Ubuntu**. Sinta-se a vontade para escolher a que se sinta mais confortável
* **Python** como linguagem de programação principal.
  Os ambientes virtuais para cada projeto são criados usando `virtualenv`
  (veja seção mais abaixo). Usamos também o `Jupyter Notebook` para
  prototipagem. Algumas da bibliotecas que usamos são:
    * `Django` e `Flask` para criação de servidores web,
    * `Pandas` para manuseio de dados tabulares,
    * ...

* **Docker** e **docker-compose** para criação de serviços. Os respectivos
  guias de instalação podem ser encontrados [aqui][docker-install] e
  [aqui][docker-compose-install],
* Banco de dados **Neo4j** para obtenção de relacionamento entre pessoas,
* Consumimos dados do banco **ElasticSearch** em produção,
* **Postman** para desenvolvimento de APIs.
* O sistema de controle de versão **git**. Se você escolheu uma versão do Linux
  compátivel com Debian (e.g., Mint e Ubuntu), [este guia][git-install] mostra
  como instalar git em sua máquina.
* Nossa IDE de preferência é o **Visual Studio Code** mas sinta-se a vontade
para usar uma em que se sinta mais confortável e produtivo.

[git-install]: https://tecadmin.net/install-git-on-ubuntu/
[docker-install]: https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository
[docker-compose-install]: https://docs.docker.com/compose/install/

## Nuvem AWS

Nosso ambiente de produção está hospedada na nuvem da **AWS** em que usamos
os seguintes recursos:

* Servidores EC2 com a AMI da AWS.
* Banco de Dados RDS MySQL.
* Banco de Dados NoSQL ElasticSearch.
* Lambdas

Temos uma planilha com as chaves de acesso aos servidores e bancos hospedados
na AWS. Peça acesso ao seu supervisor.

## Ambientes Virtuais

Para cada projeto, devemos criar um ambiente virtual do Python. As instruções
estão nos respectivos repositórios de cada projeto, mas segue um guia geral:

```sh
    git clone git@bitbucket.org:zoox-intelligence/nome-do-projeto.git
    cd nome-do-projeto
    virtualenv -p python3 .virtualenv
    source .virtualenv/bin/activate
    pip install -r requirements.txt
```

## Testes nas APIs locais

Para conseguir fazer requisição nas APIs locais do ZooxWifi, é preciso
adiciona a seguinte linha no arquivo `/etc/hosts` na sua máquina local:

> 192.168.4.252   access.api.zooxwifi.local audit.api.zooxwifi.local company.api.zooxwifi.local dashboard.api.zooxwifi.local email.api.zooxwifi.local history.api.zooxwifi.local hotspot.api.zooxwifi.local ipiranga.api.zooxwifi.local page.api.zooxwifi.local partner.api.zooxwifi.local person.api.zooxwifi.local region.api.zooxwifi.local survey.api.zooxwifi.local user.api.zooxwifi.local workflow.api.zooxwifi.local hotel.api.zooxwifi.local

## Containers para serviços de terceiros

Para certos *softwares* da pilha, podemos usar containers para o
desenvolvimeto. Abaixo, segue uma lista de comandos de como subir estes
serviços. Pode ser necessário executar os comandos usando `sudo`, mas se
quiser evitar isto, siga as instruções [deste guia][docker-nonroot].

### Neo4j

Execute o seguinte no shell ([REF][neo4j-docker]):
```sh
docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    neo4j
```

### ElasticSearch:

Execute o seguinte no shell ([REF][es-docker]):

```sh
# Cria network definida pelo usuário (útil para conectar a outros serviços
# anexados a mesma rede, como o Kibana):
docker network create somenetwork
#Run Elasticsearch:
ES_TAG=7.0.0
docker run \
    -d --name elasticsearch --net somenetwork \
    -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" \
    elasticsearch:$ES_TAG
# Verifica se subiu
curl -XGET 'localhost:9200'
```



[docker-nonroot]: https://docs.docker.com/install/linux/linux-postinstall/
[neo4j-docker]: https://hub.docker.com/_/neo4j/
[es-docker]: https://hub.docker.com/_/elasticsearch

## Colaboração

A empresa tem um Slack que pode ser acessado através deste
[link][nosso-slack]. Use seu e-mail para criar uma conta. Caso seu e-mail
esteja no domínio zooxsmart.com (por exemplo, maria@zooxsmart.com), tente utilizar
o domínio zoox.com.br.

O repositório dos projetos estão hospedados no BitBucket (acesse por este
[link][nossos-repos]) e nosso sistema de rastreamento de tíquetes é o Jira
(disponível [aqui][nosso-jira]).

[nossos-repos]: https://bitbucket.org/zoox-intelligence/
[nosso-jira]: https://zooxwifi.atlassian.net/secure/BrowseProjects.jspa
[nosso-slack]: https://zooxteam.slack.com/
